//////////////////////////////
//// Autor: Fredy Mendoza ////
//// Fecha: 20/06/2018    ////
//////////////////////////////

// Comentarios:
// Lenguaje c++

// Tiempo para generar solución: 0.025s

#include <iostream>

using namespace std;

int main()
{
    int total=0;
    for(int i=3,j=5;i<1000;i+=3,j+=5){
        total+=i;
        if(j<1000 && j%3 !=0){
            total+=j;
        }
    }
    cout << total;
}
