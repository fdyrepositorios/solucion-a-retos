//////////////////////////////
//// Autor: Fredy Mendoza ////
//// Fecha: 15/06/2018    ////
//////////////////////////////

// Comentarios: Se debe poner el archivo en
// el mismo lugar del proyecto y mover
// ultimo dato con %, al inicio.
// Lenguaje c++

// Tiempo para generar solución: 0.031s

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>

using namespace std;

int operacion(int total, int resta){
    int cantidad = total / resta;
    if(cantidad>0)
        total = total - (resta*cantidad);
return total;
}

int main()
{
    fstream archivo("datos.txt");
    string linea="";
    int total=0,resta=0,temp=0,temp2=0;
        if(!archivo.is_open())
           archivo.open("datos.txt",ios::in);
           while(getline(archivo,linea)){
                temp=atoi((linea.substr(1,linea.size())).c_str());
                if(resta>0){
                    temp2 = resta;
                }
                switch(linea[0]){
                    case '+':
                        total += temp;
                        total = operacion(total,temp2);
                        break;
                    case '*':
                        total *= temp;
                        total = operacion(total,temp2);
                        break;
                    case '%':
                        resta = temp;
                        break;
                    default:
                        total = atoi(linea.c_str());
                        break;
                }
            }

    total %= resta;
    cout << total<<"\n";
    archivo.close();
    return 0;
}
